#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
import xarray as xr
import matplotlib.pyplot as plt
from shapely.geometry import LineString
from sympy import *

import warnings
warnings.filterwarnings("ignore")

xmax = 150

A, g, w, k, h, x, z, t = symbols('A g w k h x z t')
Pot = -(A * g / w) * (cosh(k * (h + z)) / cosh(k * h)) * sin(k * x - w * t)
Eta = 1 / g * diff(Pot, t).evalf(subs={'z':0})
u = -diff(Pot, x)
w = -diff(Pot, z)
ax = -diff(u, t)
az = -diff(w, t)


def waves_dispersion(T, h):
    'Resuleve la ecuación de dispersión'
    L1 = 1
    L2 = ((9.81*T**2)/2*np.pi) * np.tanh(h*2*np.pi/L1)
    umbral = 1

    while umbral > 0.01:
        L2 = ((9.81*T**2)/(2*np.pi)) * np.tanh((h*2*np.pi)/L1)
        umbral = np.abs(L2-L1)

        L1 = L2

    L = L2
    k = (2*np.pi)/L
    c = np.sqrt(9.8*np.tanh(k*h)/k)

    return(L, k, c)

def plotWave(H, T, h, t):
    
    L, k, c = waves_dispersion(T, h)
    eta_lamb = lambdify((x), Eta.subs({'A':H/2, 'k':k, 'w':2*np.pi/T, 't': t}), 'math')
    sup = [eta_lamb(i) for i in range(xmax)]
    
    fig = plt.figure(figsize=(10,4))
    plt.fill_between(range(xmax), np.full(len(sup), -h), sup, facecolor='b', alpha=0.3, ec='b', linewidth=3)
    plt.ylim(-h, None)
    plt.xlim(0, xmax-1)
    plt.grid()
    plt.xlabel('x (m)')
    plt.ylabel('z (m)')
    hL = h/L
    if hL > 0.5: label = 'Deep Waters'
    elif hL < 1/20: label = 'Shallow Waters'
    else: label = 'Intermediate Waters'
    
    plt.title("{0}\nL = {1:.2f}m; k = {2:.2f}m-1; c = {3:.2f}m/s".format(label, L, k, c), fontweight='bold')
    
    return(fig)

def evaluate_potential(xs, zs, tt, wave_parameters):
    us = []
    ws = []
    etas = []
    
    u_lamb = lambdify((x,z,t), u.subs(wave_parameters),'math')
    w_lamb = lambdify((x,z,t), w.subs(wave_parameters),'math')
    eta_lamb = lambdify((x,z,t), Eta.subs(wave_parameters),'math')
    for xss in xs:
        for zss in zs:
            us.append(u_lamb(xss, zss, tt))
            ws.append(w_lamb(xss, zss, tt))
        etas.append(eta_lamb(xss, zss, tt))

    return np.array(us), np.array(ws), np.array(etas)


def plotVelocity(H, T, h, t):
    
    L_value, k_value, c_value = waves_dispersion(T, h)
    xs, ys = np.arange(0, xmax, 0.5), np.arange(-h, 3, 0.5)
    Xs, Ys = np.meshgrid(xs, ys)
    
    wave_parameters = {'A':H/2, 'g':9.8, 'w':2*np.pi/T, 'h':h, 'k':2*np.pi/L_value}
    us, ws, etas = evaluate_potential(xs, ys, t, wave_parameters)
    usm, wsm = us.reshape(len(xs), len(ys)), ws.reshape(len(xs), len(ys))
    ds_vel = xr.Dataset({'u':(('y', 'x'), usm.T), 'w': (('y', 'x'), wsm.T)}, coords={'x':xs, 'y':ys})

    fig, ax = plt.subplots(2, 1, figsize=(15,8), constrained_layout=True)
    
    ax[0].fill_between(xs, np.full(len(etas), -h), etas, facecolor='b', alpha=1)
    im = ax[0].pcolor(Xs, Ys, ds_vel.u, shading='auto', cmap='bwr')
    ax[0].quiver(Xs[::4, ::4], Ys[::4, ::4], ds_vel.u[::4, ::4], np.full(np.shape(Xs[::4, ::4]), 0), scale=0.5, scale_units='x')
    ax[0].fill_between(xs, etas, np.full(len(etas), H+2), facecolor='w', alpha=1)
    
    ax[0].set_ylim(-h, H+2)
    ax[0].set_xlim(0, xmax-1)
    plt.grid()
    ax[0].set_xlabel('x (m)')
    ax[0].set_ylabel('z (m)')
    plt.colorbar(im, ax=ax[0])
    ax[0].set_title('Horizontal Velocities (m/s)')
    
    ax[1].fill_between(xs, np.full(len(etas), -h), etas, facecolor='b', alpha=1)
    im = ax[1].pcolor(Xs, Ys, ds_vel.w, shading='auto', cmap='bwr')
    ax[1].quiver(Xs[::4, ::4], Ys[::4, ::4], np.full(np.shape(Xs[::4, ::4]), 0), ds_vel.w[::4, ::4], scale=0.5, scale_units='y')
    ax[1].fill_between(xs, etas, np.full(len(etas), H+2), facecolor='w', alpha=1)
    ax[1].set_ylim(-h, H+2)
    ax[1].set_xlim(0, xmax-1)
    ax[1].set_title('Vertical Velocities (m/s)')
    
    ax[1].set_xlabel('x (m)')
    ax[1].set_ylabel('z (m)')
    plt.colorbar(im, ax=ax[1])   
    ax[0].grid()
    ax[1].grid()
    
    return(fig)

def orbital_velocities(H_i, T_i, h_i, x_i, z_i):

    L_i, k_i, c_i = waves_dispersion(T_i, h_i)
    alpha = float(H_i/2 * cosh(k_i * (h_i + z_i))/sinh(k_i * h_i))
    beta = float(H_i/2 * sinh(k_i * (h_i + z_i))/sinh(k_i * h_i))

    x_elipse = [i for i in np.linspace(-alpha, alpha, 50)]
    y_elipse_p = [np.sqrt((1 - i**2/alpha**2) * beta**2) for i in x_elipse]
    y_elipse_n = [-np.sqrt((1 - i**2/alpha**2) * beta**2) for i in x_elipse]
    xor = x_elipse[25:] + x_elipse[25:][::-1] + x_elipse[:25][::-1] + x_elipse[:25]
    yor = y_elipse_p[25:] + y_elipse_n[25:][::-1] + y_elipse_n[:25][::-1] + y_elipse_p[:25]
    
    df_elip = pd.DataFrame({'x': xor, 'y':yor})#.drop_duplicates()
    df_elip['t'] = np.linspace(0, T_i, len(df_elip))    
    df_elip['x'] = df_elip.x + x_i
    df_elip['y'] = df_elip.y + z_i
    df_elip = df_elip.append({'x':df_elip.iloc[0].x, 'y':df_elip.iloc[0].y},  ignore_index=True)
    
    return(df_elip)

def closest_node(node, nodes):
    nodes = np.asarray(nodes)
    dist_2 = np.sum((nodes - node)**2, axis=1)
    return np.argmin(dist_2)

def plot_elipses(H, T, h, t):
    
    L_value, k_value, c_value = waves_dispersion(T, h)
    x_i = L_value
    z_i = np.arange(-h, 0, 2)
    
    xs, ys = np.arange(0, 2*L_value, 0.5), np.arange(-h, 3, 0.5)
    fig, ax = plt.subplots(1, figsize=(15,4))
    ax.set_aspect('equal')
    ax.set_xlim((1/2)*L_value, (3/2)*L_value)
    ax.set_xlabel('x (m)')
    ax.set_ylabel('z (m)')
    
    df_elip = orbital_velocities(H, T, h, x_i, 0)
    ax.plot(df_elip.x, df_elip.y, c='b', linewidth=0.5)
    ax.scatter(x_i, 0, c='b', s=10)
    wave_parameters = {'A':H/2, 'g':9.8, 'w':2*np.pi/T, 'h':h, 'k':2*np.pi/L_value}
    us_i, ws_i, etas_i = evaluate_potential(xs, ys, t, wave_parameters)
 
    # create elipse polygon and surface linestring
    pol_elip = LineString(list(zip(df_elip.x,df_elip.y)))
    line_eta = LineString(list(zip(xs, etas_i)))
        
    ax.fill_between(xs, np.full(len(etas_i), -h), etas_i, facecolor='b', alpha=.1)
    intersection = pol_elip.intersection(line_eta)
    
    points = [(p.x, p.y) for p in intersection]
    df_points = pd.DataFrame(points, columns=['x', 'y']).sort_values('x', ascending=True)

    if (t/T)%1 <= 0.5:
        p_in_df = closest_node((df_points.iloc[1].x, df_points.iloc[1].y), df_elip[['x', 'y']].values)
        ax.scatter(df_points.iloc[1].x, df_points.iloc[1].y, c='r')
        
    elif (t/T)%1 > 0.5: 
        p_in_df = closest_node((df_points.iloc[0].x, df_points.iloc[0].y), df_elip[['x', 'y']].values)
        ax.scatter(df_points.iloc[0].x, df_points.iloc[0].y, c='r')
    
    for z in z_i:
        df_elip = orbital_velocities(H, T, h, x_i, z)
        ax.scatter(x_i, z, c='b', s=10)
        ax.plot(df_elip.x, df_elip.y, c='b', linewidth=0.5)
        ax.scatter(df_elip.iloc[p_in_df].x, df_elip.iloc[p_in_df].y, c='r')
        
    return(fig)