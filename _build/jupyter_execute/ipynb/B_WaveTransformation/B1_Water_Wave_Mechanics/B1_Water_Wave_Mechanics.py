#!/usr/bin/env python
# coding: utf-8

# # B.1 Water Wave Mechanics

# ## Physics of waves

# In[2]:


from IPython.display import YouTubeVideo
from datetime import timedelta

start=int(timedelta(hours=0, minutes=5, seconds=0).total_seconds())
YouTubeVideo('bacwP-9osVE', width=700, height=400, start=start)


# ## Waves across the Pacific

# In[1]:


from IPython.display import YouTubeVideo
from datetime import timedelta

start=int(timedelta(hours=0, minutes=0, seconds=648).total_seconds())
YouTubeVideo('MX5cKoOm6Pk', width=700, height=400, start=start)


# In[ ]:




