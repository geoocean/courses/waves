#!/usr/bin/env python
# coding: utf-8

# # B.3 Random Wave Transformation

# ## Waves with sunset

# In[1]:


from IPython.display import YouTubeVideo
from datetime import timedelta

start=int(timedelta(hours=0, minutes=0, seconds=0).total_seconds())
YouTubeVideo('QbBsYavMHP0', width=700, height=400, start=start)


# ## Breaking waves from drone

# In[2]:


start=int(timedelta(hours=0, minutes=0, seconds=0).total_seconds())
YouTubeVideo('JflUspLN-7I', width=700, height=400, start=start)


# ## Breaking waves – oblique angle, from drone

# In[3]:


start=int(timedelta(hours=0, minutes=0, seconds=0).total_seconds())
YouTubeVideo('wbj5rcJ-es0', width=700, height=400, start=start)

start=int(timedelta(hours=0, minutes=0, seconds=0).total_seconds())
YouTubeVideo('Q56vPiikAsc', width=700, height=400, start=start)


# ## Waves – Headland, from drone

# In[4]:


start=int(timedelta(hours=0, minutes=0, seconds=0).total_seconds())
YouTubeVideo('SMxRklRloZs', width=700, height=400, start=start)

start=int(timedelta(hours=0, minutes=0, seconds=0).total_seconds())
YouTubeVideo('B2_hXgnmXF4', width=700, height=400, start=start)

