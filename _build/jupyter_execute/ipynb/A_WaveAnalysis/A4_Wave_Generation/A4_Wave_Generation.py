#!/usr/bin/env python
# coding: utf-8

# # A.4 Wave Generation

# ## Waves across the Pacific

# In[1]:


from IPython.display import YouTubeVideo
from datetime import timedelta

start=int(timedelta(hours=0, minutes=0, seconds=648).total_seconds())
YouTubeVideo('MX5cKoOm6Pk', width=700, height=400, start=start, embed=True)


# ## Deepwater wind wave growth with fetch and duration

# PDF: https://erdc-library.erdc.dren.mil/jspui/bitstream/11681/8431/1/MP-CERC-84-13.pdf
# 

# In[9]:


from IPython.display import IFrame
IFrame("../../../contents/CERC_1984.pdf", width=700, height=700)


# In[ ]:




