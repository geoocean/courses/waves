#!/usr/bin/env python
# coding: utf-8

# # Water level from Spectra

# In[1]:


import os
import os.path as op
import sys

# arrays
import numpy as np
import xarray as xr

# plots
import matplotlib.pyplot as plt

# span HTML width
from IPython.core.display import display, HTML
display(HTML("<style>.container { width:100% !important; }</style>"))

# panel interaction
import panel as pn
pn.extension() # this is very important for notebook display

sys.path.insert(0, os.path.join(os.getcwd()  , '..', '..', '..'))

# dependencies
from lib.waves import *


# ## Monochromatic waves

# In[2]:


# Define spectrum by means of the following spectral parameters
kw = {
    'wave': ['monochromatic'],
    'WL': (0, 2, 0.2, 1),
    "H": (0, 15, 1, 1), 
    "T": (1, 25, 1, 5), 
    "duration": (0, 1000, 10, 100),
    "deltat": (0.1, 2, 0.1, 0.5),
}

# plot series     
i = pn.interact(plot_waves, **kw)
    


# In[3]:


panel = pn.Column(
    pn.Column(
        i[0][1],i[0][2],i[0][3],i[0][4], 
    ),
    pn.Column(i[1])
)
panel


# ## Bichromatic waves

# In[4]:


# Define spectrum by means of the following spectral parameters
kw = {
    'wave': ['bichromatic'],
    'WL': (0, 2, 0.2, 1),
    "H": (0, 15, 1, 1), 
    "T": (1, 25, 1, 5), 
    "T2":(1, 25, 1, 6),
    "duration": (0, 1000, 10, 100),
    "deltat": (0.1, 2, 0.1, 0.1),
}

# plot series     
i = pn.interact(plot_waves, **kw)
    


# In[5]:


panel = pn.Column(
    pn.Column(
        i[0][1],i[0][2],i[0][5],i[0][3],i[0][4],
    ),
    pn.Column(i[1])
)
panel


# ## Irregular waves from JONSWAP spectra

# In[6]:


# Define spectrum by means of the following spectral parameters
kw = {
    'wave': ['Jonswap'],
    'WL': (0, 2, 0.2, 1),
    "H": (0, 15, 1, 1), 
    "T": (1, 25, 1, 5), 
    "gamma": (1, 50, 1, 3),
    "duration": (1, 1000, 10, 100),
    "deltat": (0.1, 2, 0.1, 1),
}

# plot series     
i = pn.interact(plot_waves, **kw)
    


# In[7]:


panel = pn.Column(
    pn.Column(
        i[0][1],i[0][2],i[0][5],i[0][3],i[0][4], 
    ),
    pn.Column(i[1])
)
panel


# In[ ]:




