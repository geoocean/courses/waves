#!/usr/bin/env python
# coding: utf-8

# # A.1. Observations

# In[1]:


from IPython.display import YouTubeVideo


# ## Waves hitting Cornwall

# In[2]:


YouTubeVideo('3GfNdL0E5T8', width=700, height=400)


# ## Storm from drone

# In[3]:


YouTubeVideo('LW-mWSW9kkg', width=700, height=400)


# In[ ]:




