#!/usr/bin/env python
# coding: utf-8

# # D.3 Hindcast

# ## One year of waves

# In[1]:


from IPython.display import YouTubeVideo
from datetime import timedelta

start=int(timedelta(hours=0, minutes=0, seconds=0).total_seconds())
YouTubeVideo('GfeJY8-7l7I', width=700, height=400, start=start)


# In[ ]:




