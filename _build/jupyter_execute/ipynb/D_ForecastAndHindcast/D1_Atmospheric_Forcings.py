#!/usr/bin/env python
# coding: utf-8

# # D.1 Atmospheric Forcings

# ## A year of weather

# In[1]:


from IPython.display import YouTubeVideo
from datetime import timedelta

start=int(timedelta(hours=0, minutes=0, seconds=0).total_seconds())
YouTubeVideo('wVRbeGc_6zM', width=700, height=400, start=start)


# ## One year of wind over the Atlantic

# In[2]:


start=int(timedelta(hours=0, minutes=0, seconds=0).total_seconds())
YouTubeVideo('obsw9qiBnjo', width=700, height=400, start=start)


# ## One year of wind over Asia Pacific

# In[3]:


start=int(timedelta(hours=0, minutes=0, seconds=0).total_seconds())
YouTubeVideo('pEoPNLrN7Eg', width=700, height=400, start=start)


# In[ ]:




