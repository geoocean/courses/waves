#!/usr/bin/env python
# coding: utf-8

# # C.3 Total Water Level

# ## Waves hitting Cornwall

# In[1]:


from IPython.display import YouTubeVideo
from datetime import timedelta

start=int(timedelta(hours=0, minutes=0, seconds=0).total_seconds())
YouTubeVideo('3GfNdL0E5T8', width=700, height=400, start=start)

