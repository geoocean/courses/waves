#!/usr/bin/env python
# coding: utf-8

# # C.2 Long Waves

# ## Edge waves from drone

# In[1]:


from IPython.display import YouTubeVideo
from datetime import timedelta

start=int(timedelta(hours=0, minutes=0, seconds=0).total_seconds())
YouTubeVideo('YlOIdfYIf4E', width=700, height=400, start=start)


# In[ ]:




