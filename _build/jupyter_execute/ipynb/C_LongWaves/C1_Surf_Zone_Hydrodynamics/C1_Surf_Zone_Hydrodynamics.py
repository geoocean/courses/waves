#!/usr/bin/env python
# coding: utf-8

# # C.1 Surf Zone Hydrodynamics

# ## Breaking waves time lapse

# In[1]:


from IPython.display import YouTubeVideo
from datetime import timedelta

start=int(timedelta(hours=0, minutes=0, seconds=0).total_seconds())
YouTubeVideo('_Xbn6nNAIwc', width=700, height=400, start=start)


# In[ ]:




