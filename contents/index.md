# Waves, swells and sea level

This jupyter-book is built on user-friendly and practical jupyter-notebooks related to several aspects of waves in the open ocean and coastal
regions. The books aims to provide a library of valuable tools for students and wave researchers to easily explore the nature and description of ocean waves.